package com.example.memocafe;

import android.app.Activity;    
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.Button;
import android.widget.ImageButton;

public class cafe extends Activity implements OnClickListener {
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cafe_activity);
		Intent i = this.getIntent();
		
		
		ImageButton food = (ImageButton)findViewById(R.id.food);
		food.setOnClickListener(this);
		
		ImageButton dessert = (ImageButton)findViewById(R.id.dessert);
		dessert.setOnClickListener(this);
		
		ImageButton pub = (ImageButton)findViewById(R.id.pub);
		pub.setOnClickListener(this);
		}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		
		
		int id = v.getId(); 
		 if(id == R.id.food){
			
			Intent i = new Intent(this, food.class);
			startActivity(i);
			
		}else if(id == R.id.dessert){
			
			Intent i = new Intent(this, dessert.class);
			startActivity(i);	
		
		}else if(id == R.id.pub){
			
			Intent i = new Intent(this, pub.class);
			startActivity(i);	
		
		}	
		
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
	}
}