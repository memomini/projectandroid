package com.example.memocafe;


import android.app.Activity;      
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class food extends ListActivity {
	
	DMHelper dmHelper;
	SQLiteDatabase db;
	Cursor cursor;//manage the retrieved records from the database
	SimpleCursorAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent i = this.getIntent();
		
	
		
		dmHelper = new DMHelper(this);
		db = dmHelper.getWritableDatabase();
		cursor = getAllFood();
		adapter = new SimpleCursorAdapter(this, R.layout.item, cursor, 
				new String[] {"food_name", "food_phone", "food_locate"},
				new int[] {R.id.tvname, R.id.tvtel, R.id.tvlocate}, 0);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
	}
	
	private Cursor getAllFood() {
		//db.query = execute a SELECT statement and return a cursor
		return db.query("food", //table name
				new String[] {"_id", "food_name", "food_phone", "food_locate"}, 
				null, 
				null, 
				null, //GROUP BY
				null, //HAVING
				"food_name asc");//ORDER BY
	
		}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.context, menu);//getMenuInflater().inflate(R.menu.context, menu);
	}
	
	public void onBackPressed() {
		super.onBackPressed();
		cursor.close();
		db.close();
		dmHelper.close();
		finish();
	}
	
	



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			ContentValues v = new ContentValues();
			v.put("food_name", data.getStringExtra("name"));
			v.put("food_phone", data.getStringExtra("phone"));
			v.put("food_locate", data.getStringExtra("locate"));
			
			db.insert("food", null, v);//SQL INSERT statement
			
			
			cursor = getAllFood();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
		}
		if (requestCode == 8888 && resultCode == RESULT_OK) {
			ContentValues v = new ContentValues();
			v.put("food_name", data.getStringExtra("name"));
			v.put("food_phone", data.getStringExtra("phone"));
			v.put("food_locate", data.getStringExtra("locate"));
			long pos = data.getLongExtra("pos", -1);
			
			String selection = "_id = ?";
			String[] selectionArgs = {String.valueOf(pos)};
			db.update("food", v, selection, selectionArgs);//SQL INSERT statement
			
			
			//Toast t = Toast.makeText(this, "Selected ID = "+String.valueOf(pos), Toast.LENGTH_LONG);
			//t.show();
			
			cursor = getAllFood();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_new:
			Intent i = new Intent(this, AddNew.class);
			startActivityForResult(i, 9999);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo a = (AdapterContextMenuInfo)item.getMenuInfo();
		long id = a.id;//column _id of the table
		int position = a.position;
		
		switch(item.getItemId()) {
		case R.id.action_edit:
			Cursor c = (Cursor)adapter.getItem(position);//get the record at the position
			String name = c.getString(c.getColumnIndex("food_name"));
			String phone = c.getString(c.getColumnIndex("food_phone")); 
			String locate = c.getString(c.getColumnIndex("food_locate")); 
			
			//create an intent to send data to AddNew
			Intent i = new Intent(this, AddNew.class);
			i.putExtra("name", name);
			i.putExtra("phone", phone);
			i.putExtra("locate", locate);
			
			i.putExtra("id", id);//int
			startActivityForResult(i,8888);
			//Toast t = Toast.makeText(this, "Selected ID = "+id+
			//		" with name = "+name, Toast.LENGTH_LONG);
			//t.show();
			return true;
		case R.id.action_delete:
			String selection = "_id = ?";
			String[] selectionArgs = {String.valueOf(id)};
			db.delete("food", selection, selectionArgs);
			
			cursor = getAllFood();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
			
			return true;
		}
		return super.onContextItemSelected(item);
	}
}