package com.example.memocafe;

import android.app.Activity;      
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends Activity implements OnClickListener {

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.main_activity);
		
		ImageButton bt1 = (ImageButton)findViewById(R.id.imageButton1);
		
		bt1.setOnClickListener(this);
		
		}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId(); 
		if(id == R.id.imageButton1){
					
				Intent i = new Intent(this, cafe.class);
				startActivity(i);		
		}
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
	}
}